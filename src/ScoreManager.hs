module ScoreManager(load, load', save, draw) where
    import Data.List(insert)
    import Graphics.Gloss(Picture(..), white)
    import qualified System.IO.Strict as Strict

    scoreFile :: FilePath
    scoreFile = "highscores.txt"

    draw :: [Int] -> Picture
    draw l = Pictures [ Translate (-40) (- 16 * fromIntegral i)
                      $ Scale 0.125 0.125
                      $ Color white
                      $ Text  (show i ++ ". " ++ show (reverse l !! (i - 1)))
                      | i <- [1.. (length l)]]

    load :: IO [Int]
    load = do text <- readFile scoreFile
              return $ map read (lines text)

    -- Strict loading variant
    load' :: IO [Int]
    load' = do text <- Strict.readFile scoreFile
               return $ map read (lines text)

    add :: Int -> [Int] -> [Int]
    add score list = drop (length list - 8) $ insert score list

    save :: Int -> IO ()
    save score = do list <- load'
                    save' (add score list)

    save' :: [Int] -> IO ()
    save' scores = do let strings = unlines $ map show scores
                      writeFile scoreFile strings
