module Gamestate where
    import World.World(World(..), Context(..), defaultWorld, playerDead, tryRandomSpawn, spawnEntities)

    import qualified World.Updater  as World
    import qualified World.Renderer as World
    import qualified World.Events   as World

    import qualified ScoreManager as Score

    import Graphics.Gloss
    import Graphics.Gloss.Game

    import World.Entities.Player
    import World.Entities.Enemies.Shooter

    import System.Random(StdGen, newStdGen)

    -----------------------
    -- Gamestate wrapper --
    -----------------------

    {-
     -  updateGS and drawGS always return to the wrapper so we can switch
     -  to any instance of Gamestate' at any time when updating or handling
     -  an event.
     -}

    class Gamestate' a where
        -- Update the gamestate
        updateGS :: Float -> a -> IO Gamestate
        -- Draw the gamestate
        drawGS   :: a -> IO Picture
        -- Handle the events
        eventGS  :: Event -> a -> IO Gamestate

    data Gamestate = forall a. Gamestate' a => Gamestate a
    instance Gamestate' Gamestate where
        updateGS dt (Gamestate gs) = updateGS dt gs
        drawGS      (Gamestate gs) = drawGS gs
        eventGS  ev (Gamestate gs) = eventGS ev gs

    ---------------------
    -- Title Gamestate --
    ---------------------

    data TitleGS = TitleGS
    titleGS  = Gamestate titleGS_
    titleGS_ = TitleGS

    instance Gamestate' TitleGS where
        updateGS dt = return . Gamestate
        drawGS gs = return (png "assets/title.png")
        eventGS = titleEvHandler
            where
                titleEvHandler :: Event -> TitleGS -> IO Gamestate
                titleEvHandler (EventKey (Char 'z') Down _ _) _ = playingGS
                titleEvHandler _                             gs = return $ Gamestate gs

    -----------------------
    -- Playing Gamestate --
    -----------------------

    data PlayingGS = PlayingGS
        { world :: World }

    playingGS :: IO Gamestate
    playingGS = do rng <- newStdGen
                   return $ Gamestate $ setrng rng playingGS_
                       where
                           setrng :: StdGen -> PlayingGS -> PlayingGS
                           setrng r s = s { world = (world s) { context = (context $ world s) { randomGen = r } } }
    playingGS_ = PlayingGS
        { world = gameWorld }
        where
            gameWorld :: World
            gameWorld = spawnEntities [playerObject] defaultWorld

    instance Gamestate' PlayingGS where
        updateGS dt gs =
            do let w = world gs
               w' <- World.updateGame dt w
               if playerDead w
                   then do Score.save (score $ context w)
                           return gameoverGS
                   else return $ Gamestate gs { world = w' }

        drawGS   gs =
            do let w = world gs
               World.drawWorld w

        eventGS ev gs =
            do let w = world gs
               w' <- World.eventHandler ev w
               return $ Gamestate gs { world = w' }

    ------------------------
    -- Gameover Gamestate --
    ------------------------

    data GameoverGS = GameoverGS

    gameoverGS  = Gamestate gameoverGS_
    gameoverGS_ = GameoverGS

    instance Gamestate' GameoverGS where
        updateGS dt = return . Gamestate
        drawGS gs = do s <- Score.load
                       return $ Score.draw s
        eventGS = gameoverEvHandler
            where
                gameoverEvHandler :: Event -> GameoverGS -> IO Gamestate
                gameoverEvHandler (EventKey (Char 'z') Down _ _) _ = return titleGS
                gameoverEvHandler _                             gs = return $ Gamestate gs
