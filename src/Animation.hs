module Animation where
    import Graphics.Gloss
    import Graphics.Gloss.Game(png)
    import Data.Fixed(mod')

    -- Animation [frame] time repeat
    data Animation = Animation
        { frames :: [Frame]
        , time   :: Float
        , once :: Bool
        }

    -- Frame picture length
    data Frame = Frame
        { picture :: Picture
        , framespan :: Float
        }

    -- Create a new animation from name n with l frames and frames lasting each dt seconds
    -- File format is "<name>-<int>.png" e.g "player-1.png"
    newAnimation :: String -> Integer -> Float -> Animation
    newAnimation n l dt = Animation { frames = f, time = 0, once = False }
        where
            f :: [Frame]
            f = [Frame (png ("assets/" ++ n ++ "-" ++ show i ++ ".png")) dt | i <- [0..(l - 1)]]

    -- Draw the frame at the current time.
    draw :: Animation -> Picture
    draw a = get (time a) (frames a)
        where
            get :: Float -> [Frame] -> Picture
            get _ [] = Blank
            get t (f:fs) | t > framespan f = get (t - framespan f) fs
                         | otherwise       = picture f

    -- Simple check to see if the animation has finished
    animationDone :: Animation -> Bool
    animationDone a = once a && time a > animLength a

    updateAnimation :: Float -> Animation -> Animation
    updateAnimation dt a = a { time = t'}
        where
            t, t' :: Float
            t' = if not $ once a then t `mod'` animLength a else t
            t = time a + dt

    -- Animation length
    animLength :: Animation -> Float
    animLength a = sum $ map framespan (frames a)

    -- Resets the animation
    resetAnimation :: Animation -> Animation
    resetAnimation a = a { time = 0 }
