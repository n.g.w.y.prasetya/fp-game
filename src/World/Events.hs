module World.Events where
    import Graphics.Gloss.Game(Event(..), Key(..), KeyState(..), SpecialKey(..))
    import World.World
    import System.Exit(exitSuccess)

    eventHandler :: Event -> World -> IO World
    -- Movement
    eventHandler (EventKey (SpecialKey KeyUp   ) Up   _ _) w = return (w { context = (context w) { upKey = False } })
    eventHandler (EventKey (SpecialKey KeyUp   ) Down _ _) w = return (w { context = (context w) { upKey = True } })

    eventHandler (EventKey (SpecialKey KeyLeft ) Down _ _) w = return (w { context = (context w) { leftKey = True } })
    eventHandler (EventKey (SpecialKey KeyLeft ) Up   _ _) w = return (w { context = (context w) { leftKey = False } })

    eventHandler (EventKey (SpecialKey KeyDown ) Down _ _) w = return (w { context = (context w) { downKey = True } })
    eventHandler (EventKey (SpecialKey KeyDown ) Up   _ _) w = return (w { context = (context w) { downKey = False } })

    eventHandler (EventKey (SpecialKey KeyRight) Down _ _) w = return (w { context = (context w) { rightKey = True } })
    eventHandler (EventKey (SpecialKey KeyRight) Up   _ _) w = return (w { context = (context w) { rightKey = False } })

    eventHandler (EventKey (Char 'z') Down _ _) w = return (w { context = (context w) { shootKey = True } })
    eventHandler (EventKey (Char 'z') Up   _ _) w = return (w { context = (context w) { shootKey = False } })

    eventHandler (EventKey (Char 'p') Down _ _) w = return (w { context = (context w) { paused = not (paused $ context w) } })

    -- Debug
    --eventHandler (EventKey (Char 'c') Down _ _) w@(Node _ x) =  print (getFloat2 "position" (head x)) *> return w
    --eventHandler (EventKey (Char 'v') Down _ _) w =  putStrLn (drawTree $ fmap showObjectKeys w) *> return w

    eventHandler (EventKey (SpecialKey KeyEsc) Down _ _ ) w = exitSuccess

    -- Nothing
    eventHandler _ w = return w
