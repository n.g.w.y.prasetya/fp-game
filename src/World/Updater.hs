module World.Updater (updateGame) where
    import World.Controller
    import World.Collisions
    import World.World(World(..), Entity(..), Entity'(..), Status(..), execute, cull, Context(..), tryRandomSpawn, Background(..), randomInt)
    import qualified World.Entities.Enemies.All as Enemy
    import Data.Fixed(mod')

    updateGame :: Float -> World -> IO World
    updateGame dt world
        = return
        (if not $ paused $ context world then
           doUpdateW dt
        $! doUpdateL dt
        $! cull
        $! collisions
        $! controllerUpdater dt
        $! updateBG dt
        $! rngtest world
        else world)

    updateBG :: Float -> World -> World
    updateBG dt w = w { background = bg'}
        where
            bg, bg' :: Background
            bg  = background w
            bg' = bg { backPos = backPos', starPos = starPos' }

            backPos', starPos' :: Float
            backPos' = (backPos bg + backSpd bg * dt) `mod'` 640
            starPos' = (starPos bg + starSpd bg * dt) `mod'` 640

    rngtest :: World -> World
    rngtest w = tryRandomSpawn 0.01 enemy w'
        where
            i  :: Int
            w' :: World
            (i, w') = randomInt (0, 2) w
            enemy :: Entity
            enemy = [Enemy.destroyer, Enemy.shooter, Enemy.flyer] !! i

    doUpdateL :: Float -> World -> World
    doUpdateL dt w = execute mutate w
        where
            mutate :: Entity -> Entity
            mutate = updateLocal dt

    doUpdateW :: Float -> World -> World
    doUpdateW dt w = mutate w
        where
            mutate :: World -> World
            mutate w = foldr (\m w -> m w) w mutators
            mutators :: [World -> World]
            mutators = map (updateWorld dt) $ entities w

    physics :: Float -> Entity -> Entity
    physics dt obj = (if isOutside then setStatus Dead else id) $ setPosition (x + vx * dt, y + vy * dt) obj
        where
            ( x,  y) = getPosition obj
            (vx, vy) = getVelocity obj
            isOutside = x > 200 || x < -200 || y > 200 || y < -200
