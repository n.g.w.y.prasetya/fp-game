module World.Entities.Explosion(explosion) where
    import Animation
    import World.Entities.Entity

    instance Entity' Explosion where
        -- Name
        getName       e = "Explosion"
        setName     n e = e
        -- Type
        getKind       e = Other
        setKind     k e = undefined
        -- Position
        getPosition   e = position e
        setPosition p e = e { position = p }
        -- Velocity
        getVelocity   e = (0, 0)
        setVelocity v e = e
        -- Size
        getHitbox     e = Circle 0
        setHitbox   v e = undefined
        -- Picture
        getPicture    e = draw $ animation e
        setPicture  p e = undefined
        -- Status
        getStatus     e = status e
        setStatus   s e = e { status = s }
        -- Update
        updateWorld t e = id
        updateLocal t e = e { animation = animation', status = status' }
            where
                animation' :: Animation
                animation' = updateAnimation t $ animation e
                status' :: Status
                status'    = if animationDone $ animation e then Dead else status e
        -- Health
        getHealth     e = 1
        setHealth   h e = e

    data Explosion = Explosion
        { status :: Status
        , animation :: Animation
        , position :: Position
        }

    explosion = Entity Explosion
        { status = Dying
        , animation = (newAnimation "explosion" animFrames animSpeed) { once = True }
        , position = (0, 0)
        }

    animSpeed :: Float
    animSpeed = 0.03333

    animFrames :: Integer
    animFrames = 6
