module World.Entities.Enemies.Shooter(shooterEnemy, destoyerEnemy) where
    import Graphics.Gloss.Game(png, Picture(..))
    import World.Entities.Entity
    import World.Entities.Enemies.Bullet
    import Graphics.Gloss.Data.Vector(rotateV)
    import World.World
    import Debug.Trace

    data ShootingEnemy = ShootingEnemy
        { name     :: Name
        , kind     :: Kind
        , position :: (Float, Float)
        , velocity :: (Float, Float)
        , hitbox   :: Hitbox
        , picture  :: Picture
        , status   :: Status
        , bullet   :: Entity
        , cooldown :: Float
        , cooldown' :: Float
        , health   :: Health
        , shootFunc :: Float -> ShootingEnemy -> World -> World
        }

    instance Entity' ShootingEnemy where
        -- Name
        getName         = name
        setName     n e = e { name = n }
        -- Type
        getKind         = kind
        setKind     k e = e { kind = k }
        -- Position
        getPosition     = position
        setPosition p e = e { position = p }
        -- Velocity
        getVelocity     = velocity
        setVelocity v e = e { velocity = v }
        -- Hitbox
        getHitbox       = hitbox
        setHitbox   h e = e { hitbox = h }
        -- Position
        getPicture      = picture
        setPicture  p e = e { picture = p }
        -- Status
        getStatus       = status
        setStatus   s e = e { status = s }
        -- Health
        getHealth       = health
        setHealth   h e = e { health = h }
        -- Update
        updateWorld t e w = shootFunc e t e w
        updateLocal t e = physics t $ shootL t e

    -- Shoot straight down
    shootW :: Float -> ShootingEnemy -> World -> World
    shootW t a = if p then spawnEntity b else id
        where
            b :: Entity
            b = setPosition (getPosition a) $ bullet a
            p :: Bool
            p = cooldown a <= 0

    -- Shoot towards player
    shootWSingle :: Float -> ShootingEnemy -> World -> World
    shootWSingle t a w = (if p then spawnEntity b' else id) w
        where
            b, b' :: Entity
            b' = rotateToPlayer w b
            b = setPosition (getPosition a) $ bullet a
            p = cooldown a <= 0

    -- Shoot 3x towards player like a kind of shotgun
    shootWTripple :: Float -> ShootingEnemy -> World -> World
    shootWTripple t a w = (if p then spawnEntities [b', br, bl] else id) w
        where
            b, b', br, bl :: Entity
            b' = rotateToPlayer w b
            br = setVelocity (rotateV ( 0.5) v) b'
            bl = setVelocity (rotateV (-0.5) v) b'
            b = setPosition (getPosition a) $ bullet a
            v :: Velocity
            v = getVelocity b'
            p :: Bool
            p = cooldown a <= 0

    -- Function for setting local data
    shootL :: Float -> ShootingEnemy -> ShootingEnemy
    shootL t e = if p then e { cooldown = cooldown' e } else e { cooldown = cooldown e - t}
        where
            p = cooldown e <= 0

    shooterEnemy :: Entity
    shooterEnemy = Entity ShootingEnemy
        { name     = "shooter"
        , kind     = Hostile
        , position = (0, 0)
        , velocity = (0, -32)
        , hitbox   = Circle 30
        , picture  = png "assets/shooter.png"
        , status   = Alive
        , bullet   = hostileBullet
        , health   = 5
        , cooldown  = 1
        , cooldown' = 1
        , shootFunc = shootWSingle --shootWDown
        }

    destoyerEnemy :: Entity
    destoyerEnemy = Entity ShootingEnemy
        { name     = "destroyer"
        , kind     = Hostile
        , position = (0, 0)
        , velocity = (0, -32)
        , hitbox   = Circle 30
        , picture  = png "assets/destroyer.png"
        , status   = Alive
        , bullet   = hostileBullet
        , health   = 8
        , cooldown  = 3
        , cooldown' = 3
        , shootFunc = shootWTripple --shootWDown
        }
