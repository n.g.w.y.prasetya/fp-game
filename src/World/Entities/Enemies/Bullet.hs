module World.Entities.Enemies.Bullet(hostileBullet) where
    import Graphics.Gloss.Game(png)
    import World.Entities.Entity
    import World.World
    import Graphics.Gloss.Data.Vector(Vector(..), mulSV)

    hostileBullet :: Entity
    hostileBullet
        = setName "playerProjectile"
        $ setPicture (png "assets/bullet-r.png")
        $ setKind HostileBullet
        $ setVelocity (0, -128)
        $ setHitbox (Circle 16)
        $ newEntity
