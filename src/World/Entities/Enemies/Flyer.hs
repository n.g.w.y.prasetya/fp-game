module World.Entities.Enemies.Flyer(flyerEnemy) where
    import Graphics.Gloss.Game(png)
    import World.Entities.Entity
    import World.World

    flyerEnemy :: Entity
    flyerEnemy
        = setVelocity (0, -60)
        $ setPicture (png "assets/flyer.png")
        $ setName "enemy"
        $ setKind Hostile
        $ setHitbox (Circle 30)
        $ newEntity
