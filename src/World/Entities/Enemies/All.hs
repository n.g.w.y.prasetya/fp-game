module World.Entities.Enemies.All(flyer, shooter, destroyer) where
    import World.Entities.Enemies.Flyer(flyerEnemy)
    import World.Entities.Enemies.Shooter(destoyerEnemy, shooterEnemy)

    --flyer, destroyer, shooter :: Entity
    flyer     = flyerEnemy
    destroyer = destoyerEnemy
    shooter   = shooterEnemy
