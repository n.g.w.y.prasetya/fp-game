module World.Entities.Player (playerObject) where
    import Graphics.Gloss.Game(png)
    import World.Entities.Entity
    import World.World
    import Animation

    playerObject :: Entity
    playerObject = Entity player

    data Player = Player
        { name     :: Name
        , kind     :: Kind
        , position :: Position
        , velocity :: Velocity
        , hitbox   :: Hitbox
        , pictures :: Animation
        , status   :: Status
        , health   :: Health
        }

    player :: Player
    player = Player
        { name     = "player"
        , kind     = Friendly
        , position = (0, 0)
        , velocity = (0, 0)
        , hitbox   = Circle 16
        , pictures = newAnimation "player" 4 0.167
        , status   = Alive
        , health   = 3
        }

    instance Entity' Player where
        -- Name
        getName         = name
        setName     n a = a { name = n }
        -- Type
        getKind         = kind
        setKind     k a = a { kind = k }
        -- Position
        getPosition     = position
        setPosition p a = a { position = p }
        -- Velocity
        getVelocity     = velocity
        setVelocity v a = a { velocity = v }
        -- Hitbox
        getHitbox       = hitbox
        setHitbox   s a = a { hitbox = s }
        -- Picture
        getPicture    a = draw $ pictures a
        setPicture  p a = a
        -- Status
        getStatus       = status
        setStatus   s a = a { status = s }
        -- Health
        getHealth       = health
        setHealth   h a = a { health = h }
        -- Update
        updateWorld t a = id
        updateLocal t a = bindPlayer $ physics t a { pictures = updateAnimation t $ pictures a}

    -- Prevent the player from escaping the screen and dying.
    bindPlayer :: Player -> Player
    bindPlayer p = setPosition (x', y') p
        where
            x, x', y, y' :: Float
            x' = constrain (-195, 195) x
            y' = constrain (-195, 195) y
            (x, y) = getPosition p
            constrain :: (Float, Float) -> Float -> Float
            constrain (low, high) val = min high $ max low val
