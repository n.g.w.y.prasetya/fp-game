module World.Entities.Entity(Entity'(..), Entity(..), Hitbox(..), Status(..), Kind(..), Damages(..), Name, Position, Velocity, Health, newEntity, newBasic, physics) where
    import Graphics.Gloss(Picture(Blank))
    import {-# SOURCE #-} World.World(World)
    import Graphics.Gloss.Data.Picture(Point(..))
    import Graphics.Gloss.Data.Vector(Vector(..))

    ------------
    -- Entity --
    ------------

    data Entity = forall a. Entity' a => Entity a

    class Entity' a where
        -- Name
        getName :: a -> Name
        setName :: Name -> a -> a
        -- Type
        getKind :: a -> Kind
        setKind :: Kind -> a -> a
        -- Position
        getPosition :: a -> Position
        setPosition :: Position -> a -> a
        -- Velocity
        getVelocity :: a -> Velocity
        setVelocity :: Velocity -> a -> a
        -- Size
        getHitbox :: a -> Hitbox
        setHitbox :: Hitbox -> a -> a
        -- Picture
        getPicture :: a -> Picture
        setPicture :: Picture -> a -> a
        -- Status
        getStatus :: a -> Status
        setStatus :: Status -> a -> a
        -- Damage
        getHealth  :: a -> Int
        setHealth  :: Int -> a -> a
        -- Updating
        updateLocal    :: Float -> a -> a
        updateWorld    :: Float -> a -> World -> World

    instance Entity' Entity where
        -- Name
        getName       (Entity a) = getName a
        setName     n (Entity a) = Entity $ setName n a
        -- Type
        getKind       (Entity a) = getKind a
        setKind     k (Entity a) = Entity $ setKind k a
        -- Position
        getPosition   (Entity a) = getPosition a
        setPosition p (Entity a) = Entity $ setPosition p a
        -- Velocity
        getVelocity   (Entity a) = getVelocity a
        setVelocity v (Entity a) = Entity $ setVelocity v a
        -- Size
        getHitbox     (Entity a) = getHitbox a
        setHitbox   v (Entity a) = Entity $ setHitbox v a
        -- Picture
        getPicture    (Entity a) = getPicture a
        setPicture  p (Entity a) = Entity $ setPicture p a
        -- Status
        getStatus     (Entity a) = getStatus a
        setStatus   s (Entity a) = Entity $ setStatus s a
        -- Update
        updateWorld t (Entity a) = updateWorld t a
        updateLocal t (Entity a) = Entity $ updateLocal t a
        -- Health
        getHealth     (Entity a) = getHealth a
        setHealth   h (Entity a) = Entity $ setHealth h a

    instance Eq Entity where
        (==) a b = getName a == getName b
        (/=) a b = getName a /= getName b

    -----------------
    -- BasicEntity --
    -----------------

    data BasicEntity = BasicEntity
        { name     :: Name
        , kind     :: Kind
        , position :: Position
        , velocity :: Velocity
        , hitbox   :: Hitbox
        , picture  :: Picture
        , status   :: Status
        , health   :: Health
        --, updaterL :: Float -> BasicEntity -> BasicEntity
        --, updaterW :: Float -> BasicEntity -> World -> World
        }

    instance Entity' BasicEntity where
        -- Name
        getName         = name
        setName     n a = a { name = n }
        -- Type
        getKind         = kind
        setKind     k a = a { kind = k }
        -- Position
        getPosition     = position
        setPosition p a = a { position = p }
        -- Velocity
        getVelocity     = velocity
        setVelocity v a = a { velocity = v }
        -- Hitbox
        getHitbox       = hitbox
        setHitbox   s a = a { hitbox = s }
        -- Picture
        getPicture      = picture
        setPicture  p a = a { picture = p }
        -- Status
        getStatus       = status
        setStatus   s a = a { status = s }
        --
        getHealth       = health
        setHealth   h a = a { health = h }
        -- Update
        updateWorld t a = id --updaterW a t a
        updateLocal t a = physics t a

    ------------------------
    -- Entity fields --
    ------------------------

    type Name = String
    data Kind = Friendly | FriendlyBullet | Hostile | HostileBullet | Other deriving Eq

    class Damages a where
        canDamage :: a -> a -> Bool

    instance Damages Kind where
        canDamage Hostile Friendly       = True -- Hostiles can kill the friendlies
        canDamage HostileBullet Friendly = True -- Hostile bullets can kill the friendlies
        canDamage FriendlyBullet Hostile = True -- Friendly bullets can kill hostiles
        canDamage Hostile FriendlyBullet = True -- Hostiles will destroy friendly bullets
        canDamage _ _                    = False

    type Position = Point
    type Velocity = Vector
    type Health   = Int
    data Hitbox
        = Circle Float -- Size in diametre
        -- | Square Float
        -- | Rectangle (Float, Float)
    data Status   = Alive | Dead | Dying deriving (Eq)

    ------------------
    -- Blank Entity --
    ------------------

    -- A simple implementation of entity for things that do not need
    -- complex behaviour such as bullets.
    newEntity = Entity newBasic
    newBasic  = BasicEntity
        { name     = ""
        , kind     = Friendly
        , position = (0, 0)
        , velocity = (0, 0)
        , hitbox   = Circle 8
        , picture  = Blank
        , status   = Alive
        , health   = 1
        }

    -- Physics with extra detection to see if the entity is outside the playing field
    physics :: Entity' a => Float -> a -> a
    physics dt obj = (if isOutside then setStatus Dead else id) $ setPosition (x + vx * dt, y + vy * dt) obj
        where
            ( x,  y) = getPosition obj
            (vx, vy) = getVelocity obj
            isOutside = x > 210 || x < -210 || y > 210 || y < -210
