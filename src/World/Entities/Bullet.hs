module World.Entities.Bullet (playerBullet) where
    import Graphics.Gloss.Game(png)
    import World.Entities.Entity

    playerBullet :: Entity
    playerBullet
        = setName "playerProjectile"
        $ setPicture (png "assets/bullet.png")
        $ setKind FriendlyBullet
        $ setVelocity (0, 256)
        $ setHitbox (Circle 8)
        $ newEntity
