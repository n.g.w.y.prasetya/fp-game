module World.World (
    -- Exports from World
    World(..), defaultWorld, Context(..), defaultContext, Background(..),

    -- World management
    spawnEntity, spawnEntities, execute, cull, findEntity, onEntity,
    toPlayer, rotateToPlayer, playerDead, findPlayer,

    -- Random numbers
    randomInt, randomFloat, tryRandomSpawn,

    -- Re-exports from Entity
    Entity'(..), Entity(..), Status(..), Kind(..), Damages(..), Velocity, Position
) where

    -------------
    -- Imports --
    -------------

    import Prelude
    import System.Random(StdGen, mkStdGen, Random(..))
    import Data.List(find)
    import Data.Maybe(isNothing)
    import World.Entities.Entity
    import World.Entities.Explosion
    import Graphics.Gloss(Picture(..))
    import Graphics.Gloss.Game(png)
    import Graphics.Gloss.Data.Picture(Point(..))
    import Graphics.Gloss.Data.Vector(Vector(..), normalizeV, magV, mulSV)
    import Debug.Trace

    ----------------
    -- The World --
    ----------------

    data World = World
        { entities  :: [Entity]         -- The pool of entities in the world.
        , context   :: Context          -- The context of the world. Aka globally accessable variables.
        , background :: Background
        }

    defaultWorld :: World
    defaultWorld = World
        { entities   = []             -- No entities
        , context    = defaultContext -- Default value
        , background = defaultBackground
        }

    data Background = Background
        { starPic :: Picture
        , starPos :: Float
        , starSpd :: Float
        , backPic :: Picture
        , backPos :: Float
        , backSpd :: Float
        }

    defaultBackground = Background
        { starPic = png "assets/stars.png"
        , starPos = 0
        , starSpd = 16
        , backPic = png "assets/back.png"
        , backPos = 0
        , backSpd = 8
        }

    -------------
    -- Context --
    -------------

    data Context = Context
        { randomGen :: StdGen
        , paused     :: Bool
        , shootCooldown :: Float
        , score      :: Int
        , downKey    :: Bool
        , upKey      :: Bool
        , leftKey    :: Bool
        , rightKey   :: Bool
        , shootKey   :: Bool
        }

    defaultContext = Context
        { randomGen    = mkStdGen 8008135
        , paused   = False
        , shootCooldown = 0
        , score    = 0
        , downKey  = False
        , upKey    = False
        , leftKey  = False
        , rightKey = False
        , shootKey = False
        }

    ---------------
    -- Functions --
    ---------------

    randomInt :: (Int, Int) -> World -> (Int, World)
    randomInt (a, b) world = (res, world { context = (context world) { randomGen = rng' } })
        where
            (res, rng') = randomR (a, b) rng
            rng = randomGen $ context world

    randomFloat :: World -> (Float, World)
    randomFloat world = (res, world { context = (context world) { randomGen = rng' } })
        where
            (res, rng') = randomR (0 :: Float, 1 :: Float) rng
            rng = randomGen $ context world

    tryRandomSpawn :: Float -> Entity -> World -> World
    tryRandomSpawn chance entity world = (if r1 < chance then spawnEntity (setPosition (r2 * 300 - 150, 209)entity) else id) world''
        where
            (r1, world') = randomFloat world
            (r2, world'') = randomFloat world'

    execute :: (Entity -> Entity) -> World -> World
    execute f w = w { entities = map f (entities w) }

    cull :: World -> World
    cull w = w { entities = explosions ++ onlyAlive (entities w) }
        where
            onlyAlive :: [Entity] -> [Entity]
            onlyAlive = filter (\x -> getStatus x /= Dead)
            -- Create explosions of dead entities
            explosions :: [Entity]
            explosions = map (\x -> setPosition (getPosition x) explosion) (filter (\x -> getStatus x == Dead && getKind x /= Other) $ entities w)

    findEntity :: (Entity -> Bool) -> World -> Maybe Entity
    findEntity p w = find p $ entities w

    onEntity :: Name -> (Entity -> Entity) -> World -> World
    onEntity name f w = w { entities = map mutator $ entities w}
        where
            mutator entity = (if getName entity == name then f else id) entity

    spawnEntity :: Entity -> World -> World
    spawnEntity c w = w { entities = c : entities w }

    spawnEntities :: [Entity] -> World -> World
    spawnEntities c w = w { entities = c ++ entities w }

    playerDead :: World -> Bool
    playerDead w = isNothing $ findPlayer w

    toPlayer :: World -> Entity -> Vector
    toPlayer w e = maybe (0, -1) (\posP -> normalizeV $ posP - posE) posP
        where
            posE :: Position
            posE = getPosition e
            posP :: Maybe Position
            posP = getPosition <$> findPlayer w

    findPlayer :: World -> Maybe Entity
    findPlayer = findEntity (\x -> getName x == "player")

    rotateToPlayer :: World -> Entity -> Entity
    rotateToPlayer w e = setVelocity (mulSV v d) e
        where
            d :: Vector
            d = toPlayer w e
            v :: Float
            v = magV $ getVelocity e
