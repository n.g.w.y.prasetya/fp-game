module World.Collisions(collisions) where
    import World.World
    import World.Entities.Entity
    import Prelude
    import Data.Foldable(foldl')

    {-
     - Applies function 'f' :: ((Entity, Context) -> (Entity, Context)) on
     - a tuple :: ([Entity], Context).
     -
     - It works kinda like this, with input ([x0, x1, x2], c)
     -
     -  c->-+---+->-c'------+---+->-c'------+---+->-------->-c'
     -      | f |           | f |           | f |
     - x0->-+---+->-y0 x1->-+---+->-y1 x2->-+---+->-y2
     -              |               |               |
     -              +---------------+---------------+->- [y0, y1, y2]
     -}
    applyWithContext :: ((Entity, Context) -> (Entity, Context)) -> ([Entity], Context) -> ([Entity], Context)
    applyWithContext f (xs, c) = foldl' folder ([], c) xs
        where
            folder :: ([Entity], Context) -> Entity -> ([Entity], Context)
            -- from the accummulator, append the new x' and replace the
            -- context with c'.
            folder (xs, c) x = (x':xs, c')
                where
                    x' :: Entity
                    c' :: Context
                    -- Use the context from the accummulator to calculate x', c'
                    (x', c') = f (x, c)

    collisions :: World -> World
    collisions w = w { entities = es', context = c' }
        where
            es, es' :: [Entity]
            es = entities w
            c, c' :: Context
            c  = context  w

            -- We want to have the context so we can change the score
            (es', c') = applyWithContext checkCollision (es, c)

            checkCollision :: (Entity, Context) -> (Entity, Context)
            checkCollision (e, c) = (if any (e `collidesWith`) es then handleCollision else id) (e, c)

            handleCollision :: (Entity, Context) -> (Entity, Context)
            handleCollision (e, c) = (e', c')
                where
                    -- Decrease health and set to dead if no more health
                    e' = (if h > 0 then setHealth (h - 1) else setStatus Dead) e
                    -- Increment score if enemy has been killed
                    c' = if getStatus e' == Dead && getKind e' == Hostile then c { score = s + 100 } else c
                    h = getHealth e
                    s = score c

            collidesWith :: Entity -> Entity -> Bool
            collidesWith a b = collides && isDamaged
                where
                    xA, yA, xB, yB :: Float
                    (xA, yA) = getPosition a
                    (xB, yB) = getPosition b
                    collides :: Bool
                    collides = hitboxCollision (getHitbox a) (getHitbox b)
                    isDamaged :: Bool
                    isDamaged = getKind b `canDamage` getKind a

                    -- Collision detection using circles.
                    hitboxCollision :: Hitbox -> Hitbox -> Bool
                    hitboxCollision (Circle dA) (Circle dB) = sqrt (((xA - xB) ** 2) + ((yA - yB) ** 2)) <= (dA + dB) / 2
