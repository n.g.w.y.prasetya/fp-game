module World.Controller(controllerUpdater) where
    import World.Entities.Bullet(playerBullet)
    import World.Entities.Entity
    import World.World

    playerSpeed :: Float
    playerSpeed = 128.0

    defaultShootCooldown :: Float
    defaultShootCooldown = 0.16

    controllerUpdater :: Float -> World -> World
    controllerUpdater dt w =
        (if canShoot w then shootBullet else id)
        $ onEntity "player" (setVelocity newVelocity)
        $ w { context = (context w) { shootCooldown = shootCooldown (context w) - dt } }
        where
            canShoot :: World -> Bool
            canShoot obj = shootCooldown (context w) <= 0 && shootKey (context w)

            newVelocity :: (Float, Float)
            newVelocity = (hor * playerSpeed, ver * playerSpeed)
                where
                    up    = upKey    $ context w
                    down  = downKey  $ context w
                    left  = leftKey  $ context w
                    right = rightKey $ context w
                    ver   = if up    && not down then 1 else if down && not up    then -1 else 0
                    hor   = if right && not left then 1 else if left && not right then -1 else 0

    shootBullet :: World -> World
    shootBullet w
        = spawnEntity (setPosition playerPos playerBullet)
        $ w { context = (context w) { shootCooldown = defaultShootCooldown } }
        where
            playerPos :: (Float, Float)
            playerPos = maybe (0, 0) getPosition (findPlayer w)
