module World.Renderer(drawWorld) where
    import Graphics.Gloss
    import Graphics.Gloss.Game
    import Graphics.Gloss.Data.Vector
    import World.World

    drawWorld :: World -> IO Picture
    drawWorld world = return $ Pictures [b, s, e]
        where
            b, e, s :: Picture
            b = drawBG world
            e = Pictures $ map drawEntity (entities world)
            s = Translate (-180) 180 $ Scale 0.125 0.125 $Color white $ Text s_

            s_ :: String
            s_ = show $ score $ context world

    drawBG :: World -> Picture
    drawBG w = Pictures [b1, b2, s1, s2] --[s1, s2, b1, b2]
        where
            s1, b1 :: Picture
            s1 = Translate 0 (-starPos bg)
               $ uncurry Translate (mulSV (0.02 * starSpd bg) p)
               $ Scale 2 2 $ starPic bg
            s2 = Translate 0 640 s1
            b1 = Translate 0 (-backPos bg)
               $ uncurry Translate (mulSV (0.02 * backSpd bg) p)
               $ Scale 2 2 $ backPic bg
            b2 = Translate 0 640 b1

            bg :: Background
            bg = background w

            p :: Position
            p = maybe (0, 0) getPosition $ findPlayer w

    drawEntity :: Entity -> Picture
    drawEntity entity = Translate x y sprite
        where
            (x, y) = getPosition entity
            sprite = getPicture entity
