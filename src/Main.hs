module Main where
    import Graphics.Gloss.Interface.IO.Game(Display(..), playIO)
    import Graphics.Gloss(black)
    import Gamestate(Gamestate'(..), titleGS)

    main :: IO ()
    main = do playIO (InWindow "shooter :: Functional" (400, 400) (0, 0)) black 60 titleGS drawGS eventGS updateGS
