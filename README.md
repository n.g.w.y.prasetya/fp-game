#Gameproject INFOFP
Naraenda G.W.Y. Prasetya (5832160)

## Installation
1. Extract the project.
2. Install using `cabal install`.
3. Run with `cabal run`.

# Art
The following sprites are not made by me and fall under
the Creative-Commons-Zero license and are thus public domain.

- bullet.png
- bullet-r.png
- explosion-\*.png
- destroyer.png
- shooter.png
- flyer.png
- player-\*.png

All other artworks are made by me.
